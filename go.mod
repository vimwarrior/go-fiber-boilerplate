module gitlab.com/vimwarrior/mcqp_api

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/gofiber/cors v0.0.2
	github.com/gofiber/fiber v1.8.431
	github.com/gofiber/recover v0.0.2
	github.com/o1egl/paseto v1.0.0
	github.com/urfave/cli v1.22.4
	github.com/urfave/cli/v2 v2.2.0
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
)
