package sugba

import (
	"gitlab.com/vimwarrior/mcqp_api/tula"
)

func (sugba *Sugba) WelcomeRouter() {

	WelcomeController := new(tula.WelcomeController)

	sugba.route.Get("/", WelcomeController.Welcome)

}
