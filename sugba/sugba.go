package sugba

import (
	"github.com/gofiber/fiber"
)

// Routes
type Sugba struct {
	route *fiber.App
}

func (sugba *Sugba) Init() {

	sugba.route = fiber.New()
	// Initialize Routes Here
	sugba.WelcomeRouter()

}

func (sugba *Sugba) GetRoutes() *fiber.App {
	return sugba.route
}
