package services

import (
	"fmt"
	"github.com/o1egl/paseto"
	"golang.org/x/crypto/ed25519"

	"encoding/hex"
)

type YabiConfig struct {
	PublicKey  ed25519.PublicKey
	PrivateKey ed25519.PrivateKey
	Err        error
}

type YabiData struct {
	JsonToken paseto.JSONToken
	Footer    string
}

func (config *YabiConfig) GenKey() (string, string) {
	publicKey, privateKey, _ := ed25519.GenerateKey(nil)
	skey := hex.EncodeToString(privateKey)
	pkey := hex.EncodeToString(publicKey)
	return skey, pkey
}

func (config *YabiConfig) SetPrivateKey(key string) {
	p, _ := hex.DecodeString(key)
	config.PrivateKey = ed25519.PrivateKey(p)
}

func (config *YabiConfig) SetPublicKey(key string) {
	p, _ := hex.DecodeString(key)
	config.PublicKey = ed25519.PublicKey(p)
}

func (config *YabiConfig) Sign(jsonToken paseto.JSONToken, footer string) (string, error) {
	service := paseto.NewV2()
	token, err := service.Sign(config.PrivateKey, jsonToken, footer)
	fmt.Println("Signing Error: ", err)
	return token, err
}

func (config *YabiConfig) Verify(token string) (YabiData, error) {
	var data YabiData
	service := paseto.NewV2()
	err := service.Verify(token, config.PublicKey, &data.JsonToken, &data.Footer)
	return data, err
}
