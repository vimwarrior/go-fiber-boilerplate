package server

// Get All Routes Here
import (
	"github.com/gofiber/fiber"
	"gitlab.com/vimwarrior/mcqp_api/sugba"
)

type Server struct{}

func (server *Server) InitRouters() *fiber.App {
	router := new(sugba.Sugba)
	router.Init()
	app := router.GetRoutes()
	return app
}
