package server

import (
	"github.com/gofiber/cors"
	"github.com/gofiber/fiber"
	"github.com/gofiber/recover"
	"gitlab.com/vimwarrior/mcqp_api/dbs"
	"log"
)

func Init() {
	// Initialize Server
	serve := new(Server)

	// Initialize Database
	dbs.Init()

	// Initialize Routes and Middlewares
	app := serve.InitRouters()

	// Intiialize Recovery
	cfg_recovery := recover.Config{
		Handler: func(c *fiber.Ctx, err error) {
			c.SendString(err.Error())
			c.SendStatus(500)
		},
	}
	app.Use(recover.New(cfg_recovery))

	// Initialize Cors
	app.Use(cors.New())

	// Enable prefork
	app.Settings.Prefork = true
	app.Settings.ServerHeader = "MCQP"

	err := app.Listen(2019) // Ref nCOV-19
	if err != nil {
		log.Fatal(err)
	}

}
