package tula

import (
	"github.com/gofiber/fiber"
	"log"
)

type WelcomeController struct{}

func (w *WelcomeController) Welcome(c *fiber.Ctx) {

	err := c.JSON(fiber.Map{
		"status":  200,
		"message": "Malaybalay City Quarantine Pass Tracker",
	})

	if err != nil {
		log.Fatal(err)
	}

}
