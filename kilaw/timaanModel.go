package kilaw

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/vimwarrior/mcqp_api/dbs"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Timaan struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	Token     string             `bson:"token,omitempty"`
	Active    bool               `bson:"active,omitempty"`
	CreatedAt time.Time          `bson:"created_at,omitempty"`
	UpdatedAt time.Time          `bson:"updated_at,omitempty"`
}

type TimaanCreate struct {
	Token     string    `bson:"token,omitempty"`
	Active    bool      `bson:"active,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty"`
	UpdatedAt time.Time `bson:"updated_at,omitempty"`
}

func (timaan *Timaan) GetAll() ([]*Timaan, error) {
	db := dbs.GetDatabase()

	collection := db.Collection("timaan")

	var timaan_all []*Timaan
	var err error

	cur, err := collection.Find(context.TODO(), bson.D{})
	if err != nil {
		fmt.Println("[__Database Collection Error__]")
		fmt.Println(err)
		fmt.Println("[__Database Collection Error__]")
		return nil, err
	}

	cur_err := cur.All(context.TODO(), &timaan_all)
	if cur_err != nil {
		fmt.Println("[__Database Collection Error__]")
		fmt.Println(cur_err)
		fmt.Println("[__Database Collection Error__]")
		return nil, cur_err
	}

	return timaan_all, nil
}

func (timaan *Timaan) Create(token TimaanCreate) (*mongo.InsertOneResult, error) {
	db := dbs.GetDatabase()

	collection := db.Collection("timaan")

	res, err := collection.InsertOne(context.TODO(), token)

	return res, err
}

func (timaan *Timaan) Check(token string) (Timaan, error) {
	db := dbs.GetDatabase()

	collection := db.Collection("timaan")
	var result Timaan
	err := collection.FindOne(context.TODO(), bson.M{"token": token, "active": true}).Decode(&result)

	return result, err
}
