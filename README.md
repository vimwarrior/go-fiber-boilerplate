# __ProjectName__

__ProjectName__ description/details of project


# SUTUKIL
```
  Sugba - Routes
  Tula - Controllers
  Kilaw - Models
```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them
```
golang https://golang.org/
mongodb https://www.mongodb.com/
...
```

### Installing

A step by step series of examples that tell you have to get a development environment running

Clone __ProjectName__ repository

```
git clone git@bitbucket.org:dripcreative/__projectname__.git
```

Go to __ProjectName__ Directory

```
#Note: Please be guided on how to initialize a golang project
export GOPATH=Your/gopath/Here
cd __ProjectName__
go mod download
```

Setup Environment Variables

```
# For development .env.example is already setup. Note: Please check if your mongodb is running also before going to the next step.
cp .sample-config.toml config.toml
```

Run __ProjectName__ on Development

```
go run main.go serve prefork
```
It will open in your browser http://localhost:2019/

Generate __ProjectName__ APP Key

```
go run main.go generate appkey
```

Create __ProjectName__ Admin API Key

```
go run main.go generate apikey ADMIN
```


## Deployment
Note: Update config.toml to Production Values


## Deployment to Live Server


## Built With

* [Fiber Prefork](https://fiber.wiki/) - The Framework
* [MongoDB](https://www.mongodb.com/) - Database

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](____________________). 

## Authors

* **Leivince John Marte** - [Vimwarrior.io](https://vimwarrior.io/)


