package middlewares

import (
	"github.com/gofiber/fiber"
	"gitlab.com/vimwarrior/mcqp_api/configs"
	"gitlab.com/vimwarrior/mcqp_api/kilaw"
	"gitlab.com/vimwarrior/mcqp_api/services"
)

func Authorization() func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		token := c.Get("apikey")
		timaanModel := new(kilaw.Timaan)
		_, err := timaanModel.Check(token)
		if err == nil {
			c.Next()
		} else {
			_ = c.JSON(fiber.Map{"status": false, "message": "Check Code Manual"})
			c.SendStatus(405)
		}
	}
}

func CanCan(rule string) func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		token := c.Get("apikey")

		configInit := new(configs.TomlConfig)
		config := configInit.Get()
		service := new(services.YabiConfig)
		service.SetPublicKey(config.PublicKey)

		data, err := service.Verify(token)

		if data.JsonToken.Get("rule") != rule {
			_ = c.JSON(fiber.Map{"status": false, "message": "Check Code Manual"})
			c.SendStatus(406)
			return
		}

		if err == nil {
			c.Next()
		} else {
			_ = c.JSON(fiber.Map{"status": false, "message": "Check Code Manual"})
			c.SendStatus(405)
			return
		}
	}
}
