package configs

import "github.com/BurntSushi/toml"

type TomlConfig struct {
	Name        string
	Description string
	Version     string
	Status      string
	AppKey      string `toml:"app_key"`
	PrivateKey  string
	PublicKey   string
	Prod        databaseInfo `toml:"production"`
	Dev         databaseInfo `toml:"development"`
	Database    databaseInfo
}

type databaseInfo struct {
	Host string
	Name string
}

func (config *TomlConfig) Get() *TomlConfig {
	_, _ = toml.DecodeFile("config.toml", &config)
	if config.Status == "development" {
		config.Database = config.Dev
	} else {
		config.Database = config.Prod
	}
	return config
}
