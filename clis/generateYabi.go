package clis

import (
	"fmt"
	"github.com/o1egl/paseto"
	"gitlab.com/vimwarrior/mcqp_api/configs"
	"gitlab.com/vimwarrior/mcqp_api/dbs"
	"gitlab.com/vimwarrior/mcqp_api/kilaw"
	"gitlab.com/vimwarrior/mcqp_api/services"
	"time"
)

func AppKey() {
	configInit := new(configs.TomlConfig)
	config := configInit.Get()
	service := new(services.YabiConfig)
	skey, pkey := service.GenKey()

	fmt.Println("--------------------------------------")
	fmt.Println(config.Name)
	fmt.Println("--------------------------------------")
	fmt.Println()
	fmt.Println("Private Key: ", skey)
	fmt.Println()
	fmt.Println("Public Key: ", pkey)
	fmt.Println()
}

func SecretKey() {
	configInit := new(configs.TomlConfig)
	config := configInit.Get()
	service := new(services.YabiConfig)
	skey, pkey := service.GenKey()

	fmt.Println("--------------------------------------")
	fmt.Println(config.Name)
	fmt.Println("--------------------------------------")
	fmt.Println()
	fmt.Println("Secret Key: ", skey)
	fmt.Println()
	fmt.Println("App Key: ", pkey)
	fmt.Println()
}

func ApiKey(rule string) {
	dbs.Init()
	configInit := new(configs.TomlConfig)
	config := configInit.Get()
	service := new(services.YabiConfig)
	service.SetPrivateKey(config.PrivateKey)

	jsonToken := paseto.JSONToken{
		Expiration: time.Now().Add(8766 * time.Hour),
	}

	jsonToken.Set("purpose", "AppKey")
	jsonToken.Set("issuer", "CLI Generator")
	jsonToken.Set("rule", rule)

	token, _ := service.Sign(jsonToken, "CLI Generator")

	timaanModel := new(kilaw.Timaan)

	data := kilaw.TimaanCreate{
		Token:     token,
		Active:    true,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	res, _ := timaanModel.Create(data)

	fmt.Println("--------------------------------------")
	fmt.Println(config.Name)
	fmt.Println("--------------------------------------")
	fmt.Println()
	fmt.Println("Api Key: ", token)
	fmt.Println()
	fmt.Println("Response: ", res)
	fmt.Println()
}

func TestApiKey(token string) {
	dbs.Init()
	configInit := new(configs.TomlConfig)
	config := configInit.Get()
	service := new(services.YabiConfig)
	service.SetPublicKey(config.PublicKey)

	timaanModel := new(kilaw.Timaan)

	result, err := timaanModel.Check(token)

	data, verr := service.Verify(token)

	fmt.Println("--------------------------------------")
	fmt.Println(config.Name)
	fmt.Println("--------------------------------------")
	fmt.Println()
	fmt.Println("Check Result: ", result)
	if err != nil {
		fmt.Println("Check Error: ", err)
	}
	fmt.Println()
	fmt.Println("Verified Data: ", data)
	if verr != nil {
		fmt.Println("Verified Error: ", verr)
	}
	fmt.Println()
	fmt.Println()
}
