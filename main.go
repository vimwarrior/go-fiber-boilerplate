package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/vimwarrior/mcqp_api/clis"
	"gitlab.com/vimwarrior/mcqp_api/server"
	"log"
	"os"
)

func main() {
	app := &cli.App{
		Usage: "MCQP Server CLI",
		Commands: []*cli.Command{
			{
				Name:    "serve",
				Aliases: []string{"s"},
				Usage:   "Serve API",
				Action: func(c *cli.Context) error {
					if _, err := os.Stat("config.toml"); err == nil {
						server.Init()
					} else {
						fmt.Println("Please create a config.toml file see sample-config.toml")
					}
					return nil
				},
			},
			{
				Name:    "generate",
				Aliases: []string{"g"},
				Usage:   "options for task templates",
				Subcommands: []*cli.Command{
					{
						Name:    "appkey",
						Usage:   "Generate Application Key",
						Aliases: []string{"ak"},
						Action: func(c *cli.Context) error {
							clis.AppKey()
							return nil
						},
					},
					{
						Name:    "secretkey",
						Aliases: []string{"sk"},
						Usage:   "Generate Secret Key",
						Action: func(c *cli.Context) error {
							clis.AppKey()
							return nil
						},
					},
					{
						Name:    "apikey",
						Usage:   "Generate API Key",
						Aliases: []string{"pi"},
						Action: func(c *cli.Context) error {
							clis.ApiKey(c.Args().First())
							return nil
						},
					},
					{
						Name:    "testapikey",
						Usage:   "Test API KEY",
						Aliases: []string{"tpi"},
						Action: func(c *cli.Context) error {
							clis.TestApiKey(c.Args().First())
							return nil
						},
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
