package dbs

import (
	"context"
	"fmt"
	"gitlab.com/vimwarrior/mcqp_api/configs"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var db *mongo.Client
var err error
var config *configs.TomlConfig

func Init() {
	configInit := new(configs.TomlConfig)
	config = configInit.Get()

	db, err = mongo.Connect(context.TODO(), options.Client().ApplyURI(config.Database.Host))
	if err != nil {
		fmt.Println("[__Database Error__]")
		fmt.Println(err)
		fmt.Println("[__Database Error__]")
	}
}

func GetDatabase() *mongo.Database {
	return db.Database(config.Database.Name)
}
